﻿CREATE TABLE [dbo].[Students] (
    [StudentID]               INT            IDENTITY (1, 1) NOT NULL,
    [FirstName]               NVARCHAR (MAX) NOT NULL,
    [LastName]                NVARCHAR (MAX) NOT NULL,
    [Gender]                  NVARCHAR (MAX) NOT NULL,
    [GPA]                     DECIMAL (3, 2) NOT NULL,
    [HasUnpaidParkingTickets] BIT            NOT NULL,
    [DepartmentID]            INT            NULL,
    PRIMARY KEY CLUSTERED ([StudentID] ASC),
    CONSTRAINT [fk_PersonDepartment] FOREIGN KEY ([DepartmentID]) REFERENCES [dbo].[Departments] ([DepartmentID])
);

