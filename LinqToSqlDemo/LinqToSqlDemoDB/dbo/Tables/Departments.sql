﻿CREATE TABLE [dbo].[Departments] (
    [DepartmentID]   INT            IDENTITY (1, 1) NOT NULL,
    [DepartmentName] NVARCHAR (MAX) NOT NULL,
    PRIMARY KEY CLUSTERED ([DepartmentID] ASC)
);

