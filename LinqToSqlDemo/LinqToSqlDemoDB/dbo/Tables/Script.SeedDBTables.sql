﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

IF NOT EXISTS (SELECT * FROM [dbo].[Departments]) INSERT INTO [dbo].[Departments]
(DepartmentName) VALUES
('Information Systems'),
('Early Childhood Development'),
('Accounting')
GO

IF NOT EXISTS (SELECT * FROM [dbo].[Students]) INSERT INTO [dbo].[Students] (FirstName, LastName, Gender, GPA, HasUnpaidParkingTickets, DepartmentID)
VALUES ('Alex', 'Jones', 'Male', 3.56, 1, 1),
	   ('Jennefer', 'Bazaldua', 'Female', 2.52, 0, 2),
	   ('Michale', 'Gasser', 'Male', 3.3, 1, 3),
	   ('Wayne', 'Pittmon', 'Male', 2.77, 0, 1),
	   ('Brittanie', 'Fountaine', 'Female', 3.01, 1, 2),
	   ('Carter', 'Bickerstaff', 'Male', 3.49, 0, 3),
	   ('Josefina', 'Daubert', 'Female', 3.41, 1, 1),
	   ('Erlinda', 'Mars', 'Female', 2.51, 0, 2)
GO