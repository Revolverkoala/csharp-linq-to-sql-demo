﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LinqToSqlDemoASP
{
    public partial class SampleApp : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        private void GetData()
        {
            var dbContext = new SampleDataContext();
            GridView1.DataSource = dbContext.Students;

            //GridView1.DataSource = from student in dbContext.Students
            //    where student.Gender == "Male"
            //    orderby student.GPA descending
            //    select student;


            GridView1.DataBind();
        }

        protected void btnGetData_Click(object sender, EventArgs e)
        {
            GetData();
        }

        protected void btnInsert_Click(object sender, EventArgs e)
        {
            using (var dbContext = new SampleDataContext())
            {
                var newStudent = new Student
                {
                    FirstName = "Tim",
                    LastName = "T",
                    Gender = "Male",
                    GPA = 3.47m,
                    DepartmentID = 3
                };

                dbContext.Students.InsertOnSubmit(newStudent);
                dbContext.SubmitChanges();
            }

            GetData();
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            using (var dbContext = new SampleDataContext())
            {
                var student = dbContext.Students.SingleOrDefault(x => x.StudentID == 9);

                if (student != null)
                {
                    student.GPA = 3.68m;
                    dbContext.SubmitChanges();
                }
            }

            GetData();
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            using (var dbContext = new SampleDataContext())
            {
                var student = dbContext.Students.SingleOrDefault(x => x.StudentID == 9);

                if (student != null)
                {
                    dbContext.Students.DeleteOnSubmit(student);
                    dbContext.SubmitChanges();
                }
            }

            GetData();
        }
    }
}